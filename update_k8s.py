#!/usr/bin/env python3

import os
import subprocess
import yaml


final_kube_config = {
        'apiVersion': 'v1',
        'clusters': [],
        'contexts': [],
        'current-context': '',
        'kind': 'Config',
        'preferences': {},
        'users': []
    }

k8s_clusters = []

try:
    with open("{}/.kube/config".format(os.environ['HOME'])) as f:
        print("☸️  read Kubernetes config")
        final_kube_config = yaml.load(f, Loader=yaml.FullLoader)
except OSError:
    print("⚠️  Kubernetes config file, doesn't exists, will create one")


with open("{}/.kube/clusters.yaml".format(os.environ['HOME'])) as f:
    print("🔧 read cluster config file")
    k8s_clusters = yaml.load(f, Loader=yaml.FullLoader)

for cluster_addr, metadatas in k8s_clusters.items():
    try:
        subprocess.run([
            "scp", "-o PasswordAuthentication=no", "-o ConnectTimeout=10",
            "{}:.kube/config".format(cluster_addr), "/tmp/kube_config"],
            check=True, capture_output=True)
        print("⏭️  processing kube config of {}:".format(cluster_addr), end = '')
        with open('/tmp/kube_config') as kube_config_file:
            kube_config = yaml.load(kube_config_file, Loader=yaml.FullLoader)
            cluster_names_replacement = {}
            user_names_replacement = {}
            if len(kube_config['clusters']) > 0:
                print(" ☸️ :", end='')
            for cluster in kube_config['clusters']:
                cluster_names_replacement[cluster['name']] = "{}-{}".format(
                    metadatas['name'], cluster['name'])
                new_cluster = {
                    'name': cluster_names_replacement[cluster['name']],
                    'cluster': cluster['cluster']
                }
                if "local_port" in metadatas:
                    new_cluster['cluster']['server'] = "https://127.0.0.1:{}".format(
                        metadatas["local_port"])
                cluster_not_found = True
                for idx, present_cluster in enumerate(final_kube_config['clusters']):
                    if present_cluster['name'] == new_cluster['name']:
                        print("🔃", end='')
                        cluster_not_found = False
                        final_kube_config['clusters'][idx]['cluster'] = new_cluster['cluster']
                if cluster_not_found:
                    print("⬆️ ", end='')
                    final_kube_config['clusters'].append(new_cluster)
            if len(kube_config['users']) > 0:
                print(" 👤:", end='')
            for user in kube_config['users']:
                user_names_replacement[user['name']] = "{}-{}".format(
                    metadatas['name'], user['name'])
                new_user = {
                    'name': user_names_replacement[user['name']],
                    'user': user['user']
                }
                user_not_found = True
                for idx, present_user in enumerate(final_kube_config['users']):
                    if present_user['name'] == new_user['name']:
                        print("🔃", end='')
                        user_not_found = False
                        final_kube_config['users'][idx]['user'] = new_user['user']
                if user_not_found:
                    print("⬆️ ", end='')
                    final_kube_config['users'].append(new_user)
            if len(kube_config['users']) > 0:
                print(" 🏷️ :", end='')
            for idx, context in enumerate(kube_config['contexts']):
                new_context = {
                    'name': "{}-{}".format(
                        user_names_replacement[context['context']['user']],
                        cluster_names_replacement[context['context']['cluster']]
                    ),
                    'context': {
                        'cluster': cluster_names_replacement[context['context']['cluster']],
                        'user': user_names_replacement[context['context']['user']]
                    }
                }
                if idx == 0:
                    new_context["name"] = metadatas["name"]
                context_not_found = True
                for internal_idx, present_context in enumerate(final_kube_config['contexts']):
                    if present_context['name'] == new_context['name']:
                        print("🔃", end='')
                        context_not_found = False
                        final_kube_config['contexts'][internal_idx]['context'] = new_context['context']
                if context_not_found:
                    print("⬆️ ", end='')
                    final_kube_config['contexts'].append(new_context)
                if not final_kube_config['current-context']:
                    print("🔼", end='')
                    final_kube_config['current-context'] = "{}@{}".format(
                        user_names_replacement[context['context']['user']],
                        cluster_names_replacement[context['context']['cluster']]
                    )
        print('')

    except subprocess.CalledProcessError:
        print("⚠️  cluster {} is not reachable, ignoring".format(cluster_addr))

print ("🗑️ clean files")
subprocess.run(["rm", "-rf", "/tmp/kube_config"], capture_output=True)
with open("{}/.kube/config".format(os.environ['HOME']), 'w') as f:
    print("📝☸️  Writing kube config file")
    data = yaml.dump(final_kube_config, f)
