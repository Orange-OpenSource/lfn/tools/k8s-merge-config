# Kubernetes Merge Config

Let's say you have several ephemeral kubernetes clusters. And maybe these 
clusters are behind firewalls / jumphosts, ...

This script allows you to retrieve the `.kube/config` of your different 
clusters, parse them and add to a global config.

Furthermore, it can transform the server URL into a local one, for example
if you're using `ssh` to create tunnels to these clusters

## Prerequisites

- Python 3.5+
- scp
- rm

## Usage

create a file in your `~/.kube` folder named `servers.yaml` with following
structure:

```yaml
---
cluster1.where.it.is:
  name: cluster1
  local_port: 6443
192.168.7.5:
  name: cluster2
  local_port: 6444
my.gke.cluster:
  name: gke
```

In the example `cluster1.where.it.is`, `192.168.7.5` and `my.gke.cluster` are 
the name you'll use to connect to them via ssh.

so configure your `~/.ssh/config` file if needed

the two first clusters have a key named `local_cluster` with a numerical value.
It's because we connect to them for Kubernetes using a local proxy.

so before being able to use `kubectl` (and any tools using kubernetes config 
file) with them, you must create a local proxy with SSH:

```shell
$ ssh -L 6443:193.5.6.7:6443 -L 6444:192.168.7.5:6443 my_jumphost
```

with `193.5.6.7` the IP address from `cluster1` which is reachable from the 
jumphost `my_jumphost`

as it's pure ssh, you obviously can use `ProxyCommand` or `ProxyJump` to reach
`my_jumphost`.

now you can generate your config file:

```shell
$ ./update_k8s.py
```

once it's finished, you can use context from kubectl (`kubectl --context 
cluster1 get po`) or use [kubectx and kubens](https://github.com/ahmetb/kubectx)
to simplify your life.

## Things to know

* If you have several clusters/users in one of the retrieved `~/.kube/config`, 
  the first context will be named with the value put in name, the others will be
  named `value_in_name-user.name-cluster.name`.
* We support only Proxy on the machine (so use ssh)
* We retrieve the config on the target on `~/.kube/config`. If it's elsewhere, 
  it won't work.
* __But__ the target machine has no need to be part of the cluster, just having
 the config file
